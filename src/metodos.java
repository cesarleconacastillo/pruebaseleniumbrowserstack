import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class metodos {
	
	public static void capturaPantalla(WebDriver driver, String imageName) {
		File directory = new File("/Users/admin/Documents/workspace/Allianz");

		try {
			if (directory.isDirectory()) {
				File imagen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(imagen, new File(directory.getAbsolutePath() + "/" + "Evidencia" + "/" + imageName + ".jpg"));
			} else {
				throw new IOException("ERROR : La  especificada no es un directorio!");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
