import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Principal {
	
static List<String> AgregarInfo=new ArrayList<String>();
public static final String USERNAME = System.getenv("USERNAME");
public static final String AUTOMATE_KEY = System.getenv("AUTOMATE_KEY") ;
public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	
 public static void main(String[] args) throws Exception {
	 
	 

    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("browser", "chrome");
    caps.setCapability("browserstack.debug", "true");
    caps.setCapability("build", "First build");
    System.setProperty("webdriver.chrome.driver", "/Users/admin/Downloads/chromedriver-2");
    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);

    driver.get("https://www.allianz.com.mx/");
    driver.findElement(By.xpath("//*[@id='layout_1']/span")).click();
    metodos.capturaPantalla(driver, "captura1");
	AgregarInfo.add("captura1");
    Thread.sleep(1000);
    driver.findElement(By.xpath("//*[@id='layout_10']/a")).click();
    metodos.capturaPantalla(driver, "captura2");
    AgregarInfo.add("captura2");
    Thread.sleep(1000);
    driver.findElement(By.xpath("//*[@id='btn-orng-crsl']/span[1]")).click();
    metodos.capturaPantalla(driver, "captura3");
    AgregarInfo.add("captura3");
    
    GenerarDocumentoPDF.GenerarPDF(AgregarInfo);
    System.out.println(driver.getTitle());
    driver.quit();

 }
}
