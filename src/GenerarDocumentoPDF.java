import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

	public class GenerarDocumentoPDF {

		public static  void GenerarPDF(List<String> testCase) throws DocumentException, FileNotFoundException {
			
			Document documento = new Document();
			File directory = new File("/Users/admin/Documents/workspace/Allianz/");
			FileOutputStream ficheroPdf = new FileOutputStream("Evidencia  Allianz Test Correcto.pdf");
			final int numberoLineas = testCase.size();
			PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
			documento.open();
			try{
				for (int i = 0; i < numberoLineas ; i++) {
			        String nombre = testCase.get(i).toString();
				    Image foto = Image.getInstance(directory.getAbsolutePath() + "/" + "Evidencia" + "/" +nombre + ".jpg");
				    foto.scaleToFit(500, 500);
				    foto.setAlignment(Chunk.ALIGN_LEFT);
				    documento.add(foto);
				 //   documento.add(new Paragraph("El test de: "+ nombre + " ha sido correcto" ,FontFactory.getFont("arial",10,Font.ITALIC,BaseColor.GREEN.darker())));
				}  
			}catch ( Exception e ){
				  throw new RuntimeException(e);
			}
			documento.close();	
		}
		}