

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
    private static ExtentReports extent;
    
    public static ExtentReports getInstance() {
        if (extent == null) {
            extent = new ExtentReports("C:\\Users\\cemorale\\TestMotorOnLine\\MotorOnLineTest\\ReporteMotorOnline.html", true);
            extent.config()
                .documentTitle("Reporte de Automatizacion")
                .reportName("Motor Online")
                .reportHeadline("");           
            extent
                .addSystemInfo("Selenium Version", "2.46")
                .addSystemInfo("Environment", "QA");
        }
        return extent;
    }
}